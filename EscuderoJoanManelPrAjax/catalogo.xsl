<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes"/>
  <xsl:template match="/">

    <xsl:element name="section">
      <xsl:attribute name="class">videojuegos</xsl:attribute>
      <xsl:attribute name="id">videojuegos</xsl:attribute>
      <xsl:element name="h3">VIDEOJUEGOS</xsl:element>
      <xsl:element name="div">
        <xsl:attribute name="class">videoJ</xsl:attribute>
        <xsl:for-each select="Videojuegos/Videojuego">
          <xsl:element name="article">
            <xsl:element name="img">
              <xsl:attribute name="src">
                <xsl:value-of select="InfoGeneral/Foto"/>
              </xsl:attribute>
              <xsl:attribute name="class">imagen</xsl:attribute>
            </xsl:element>
            <xsl:element name="h3">
              <xsl:value-of select="Nombre"/>
            </xsl:element>
            <xsl:element name="div">
              <xsl:attribute name="class">texto</xsl:attribute>
              <xsl:element name="p">
                <xsl:value-of select="InfoGeneral/Descripcion"/>
              </xsl:element>
            </xsl:element>
            <xsl:element name="div">
              <xsl:attribute name="class">button</xsl:attribute>
              <xsl:element name="div">
                <xsl:element name="a">
                  <xsl:attribute name="href">detalle.html?id=<xsl:value-of select="@idVideoJuego"/></xsl:attribute>Mas Info</xsl:element>
              </xsl:element>
            </xsl:element>
          </xsl:element>
        </xsl:for-each>
      </xsl:element>
      </xsl:element>
    </xsl:template>
  </xsl:stylesheet>