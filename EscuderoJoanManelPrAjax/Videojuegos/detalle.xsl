<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes"/>
  <xsl:template match="/Videojuego">

      <xsl:element name="article">
        <xsl:attribute name="class">article</xsl:attribute>
        <xsl:element name="h2"><xsl:value-of select="Nombre"/></xsl:element>
        <xsl:element name="p"><xsl:value-of select="InfoGeneral/Descripcion"/></xsl:element> 
      </xsl:element>
      <xsl:element name="article"><xsl:attribute name="class">imag</xsl:attribute>
      <xsl:element name="img"><xsl:attribute name="src"><xsl:value-of select="InfoGeneral/Foto"/></xsl:attribute></xsl:element>
      <xsl:element name="div"><xsl:attribute name="class">imagenes</xsl:attribute>
        <xsl:element name="h5">Desarrollador: <xsl:value-of select="InfoGeneral/Desarrollador"/></xsl:element>
        <xsl:element name="img"><xsl:attribute name="src"><xsl:value-of select="InfoGeneral/Desarrollador/@Logo"/></xsl:attribute></xsl:element>
        <xsl:element name="h5">Distribuidor: <xsl:value-of select="InfoGeneral/Distribuidor"/></xsl:element>        
        <xsl:element name="img"><xsl:attribute name="src"><xsl:value-of select="InfoGeneral/Distribuidor/@Logo"/></xsl:attribute></xsl:element>
      </xsl:element>
      </xsl:element>
      <xsl:element name="article">
        <xsl:attribute name="class">article</xsl:attribute>
        <xsl:element name="ul"><xsl:element name="li"><xsl:element name="span">Plataformas:</xsl:element></xsl:element>
          <xsl:element name="ol"><xsl:for-each select="Plataformas/Plataforma"><xsl:element name="li"><xsl:value-of select="."/></xsl:element></xsl:for-each></xsl:element>
        </xsl:element>
        <xsl:element name="ul"><xsl:element name="li"><xsl:element name="span">Modos de juego:</xsl:element></xsl:element>
          <xsl:element name="ol"><xsl:for-each select="ModosJuego/ModoJuego"><xsl:element name="li"><xsl:value-of select="."/></xsl:element></xsl:for-each></xsl:element>
        </xsl:element>
        <xsl:element name="article"><xsl:attribute name="class">article</xsl:attribute><xsl:element name="img"><xsl:attribute name="src"><xsl:value-of select="Foto1"/></xsl:attribute></xsl:element>
        <xsl:element name="img"><xsl:attribute name="src"><xsl:value-of select="Foto2"/></xsl:attribute></xsl:element>
        <xsl:element name="img"><xsl:attribute name="src"><xsl:value-of select="Foto3"/></xsl:attribute></xsl:element></xsl:element>
      </xsl:element>
    </xsl:template>
  </xsl:stylesheet>