<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>    
<xsl:template match="/root/prediccion">
<html>
       <body>
            <h1>Prevision del tiempo</h1>
            <table style="border: 1px solid black">
                <tr style="background-color:lightblue">
                    <td style="text-align:left">Fecha</td>
                    <td style="text-align:left">Maxima</td>
                    <td style="text-align:left">Minima</td>
                    <td style="text-align:left">Prediccion</td>
                </tr>
                <xsl:for-each select="dia">
                <xsl:sort select="temperatura/maxima" order = "descending"  />
                <tr style="background-color:lightblue">
                    <td style="border: 1px solid black"><xsl:value-of select="@fecha"></xsl:value-of></td>
                    <td style="border: 1px solid black"><xsl:value-of select="temperatura/maxima"></xsl:value-of></td>
                    <td style="border: 1px solid black"><xsl:value-of select="temperatura/minima"></xsl:value-of></td>
                    <td style="border: 1px solid black"><img src="{concat('imagenes/',estado_cielo/@descripcion)}.png"/></td>
                </tr>
                </xsl:for-each>
            </table>
       </body>
</html>
</xsl:template>
</xsl:stylesheet>
