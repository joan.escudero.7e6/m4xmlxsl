<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/evaluacion">
        <html>
            <body>
                <h1>Evaluacion</h1>
                <table style="border: 1px solid black">
                    <tr style="background-color:lightgreen">
                        <td style="text-align:left; font-weight:bold">Imagen</td>
                        <td style="text-align:left; font-weight:bold">Nombre</td>
                        <td style="text-align:left; font-weight:bold">Apellidos</td>
                        <td style="text-align:left; font-weight:bold">Telefono</td>
                        <td style="text-align:left; font-weight:bold">Repetidor</td>
                        <td style="text-align:left; font-weight:bold">Nota Practica</td>
                        <td style="text-align:left; font-weight:bold">Nota Examen</td>
                        <td style="text-align:left; font-weight:bold">Nota Total</td>
                    </tr>
                    <xsl:for-each select="alumno">
                        <xsl:sort select="apellidos" order = "ascending" />
                        <tr>
                            <xsl:choose>
                                <xsl:when test="@foto='si'">
                                    <td>
                                        <xsl:element name="img">
                                            <xsl:attribute name="src">
                                                imagenes/<xsl:value-of select="nombre"/>.png
                                            </xsl:attribute>
                                            <xsl:attribute name="width">
                                                100
                                            </xsl:attribute>
                                            <xsl:attribute name="height">
                                                100
                                            </xsl:attribute>
                                        </xsl:element>
                                    </td>
                                </xsl:when>
                                <xsl:otherwise>
                                    <td>
                                        <xsl:element name="img">
                                            <xsl:attribute name="src">
                                                imagenes/nacho.png
                                            </xsl:attribute>
                                            <xsl:attribute name="width">
                                                100
                                            </xsl:attribute>
                                            <xsl:attribute name="height">
                                                100
                                            </xsl:attribute>
                                        </xsl:element>
                                    </td>
                                </xsl:otherwise>
                            </xsl:choose>


                            <td style="border: 1px solid black">
                                <xsl:value-of select="nombre"></xsl:value-of>
                            </td>
                            <td style="border: 1px solid black">
                                <xsl:value-of select="apellidos"></xsl:value-of>
                            </td>
                            <td style="border: 1px solid black">
                                <xsl:value-of select="telefono"></xsl:value-of>
                            </td>
                            <td style="border: 1px solid black">
                                <xsl:value-of select="@repite"></xsl:value-of>
                            </td>
                            <xsl:apply-templates select="notas"/>
                        </tr>
                    </xsl:for-each>

                </table>

            </body>
        </html>
    </xsl:template>
    <xsl:template match="notas">
        <td style="border: 1px solid black">
            <xsl:value-of select="practicas"></xsl:value-of>
        </td>
        <td style="border: 1px solid black">
            <xsl:value-of select="examen"></xsl:value-of>
        </td>
        <xsl:choose>
            <xsl:when test="(practicas + examen) div 2 &gt; 8">
                <td style="background-color:lightblue;border: 1px solid black">
                    <xsl:value-of select="(practicas + examen) div 2"></xsl:value-of>
                </td>
            </xsl:when>
            <xsl:when test="(practicas + examen) div 2 &lt; 5">
                <td style="background-color:red;border: 1px solid black">
                    <xsl:value-of select="(practicas + examen) div 2"></xsl:value-of>
                </td>
            </xsl:when>
            <xsl:otherwise>
                <td style="border: 1px solid black">
                    <xsl:value-of select="(practicas + examen) div 2"></xsl:value-of>
                </td>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>