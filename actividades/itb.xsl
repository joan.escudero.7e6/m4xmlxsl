<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>    
<xsl:template match="/">
   <html>
       <body>
        <xsl:apply-templates select="itb/profes"/>
        <xsl:apply-templates select="itb/aula"/>
       </body>
    </html>
       
    </xsl:template>


    <xsl:template match="itb/aula">
    <h1>El aula es: <xsl:value-of select="@num"></xsl:value-of></h1>
    <p>Mis alumnos son:</p>
    <ul>
        <xsl:for-each select="alumno">
            <xsl:sort select="nombre"/>
            <li><xsl:value-of select="nombre"></xsl:value-of></li>
        </xsl:for-each>
    </ul>
</xsl:template>

<xsl:template match="itb/profes">
    <p>¿Cuantos profes hay en el itb? <xsl:value-of select="."></xsl:value-of></p>
</xsl:template>
</xsl:stylesheet>