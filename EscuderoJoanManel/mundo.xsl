<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/mundo">
        <html>
            <body>
                <xsl:for-each select="continente">
                    <xsl:element name="h1">
                        <xsl:attribute name="font-weight">bold</xsl:attribute>
                        <xsl:value-of select="nombre"/>
                    </xsl:element>
                    <table style="{concat('border: 1px solid ',nombre/@color)} ;">
                        <tr>
                            <td style="{concat('border: 1px solid ',nombre/@color)} ; font-weight:bold">Bandera</td>
                            <td style="{concat('border: 1px solid ',nombre/@color)} ; font-weight:bold">Pais</td>
                            <td style="{concat('border: 1px solid ',nombre/@color)} ; font-weight:bold">Gobierno</td>
                            <td style="{concat('border: 1px solid ',nombre/@color)} ; font-weight:bold">Capital</td>
                        </tr>
                        <xsl:apply-templates select="paises"/>
                    </table>
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>


    <xsl:template match="/mundo/continente/paises">
        <xsl:for-each select="pais">
            <xsl:sort select="nombre" order = "ascending"/>
            <tr>
                <td style="{concat('border: 1px solid ',../../nombre/@color)}">
                    <xsl:element name="img">
                        <xsl:attribute name="src">img/<xsl:value-of select="foto"/>
                        </xsl:attribute>
                    </xsl:element>
                </td>
                <td style="{concat('border: 1px solid ',../../nombre/@color)}">
                    <xsl:value-of select="nombre"/>
                </td>
                <xsl:choose>
                    <xsl:when test="nombre/@gobierno = 'dictadura'">
                        <td style="{concat('border: 1px solid ',../../nombre/@color)};background-color: red;">
                            <xsl:value-of select="nombre/@gobierno"/>
                        </td>
                    </xsl:when>
                    <xsl:when test="nombre/@gobierno = 'monarquia'">
                        <td style="{concat('border: 1px solid ',../../nombre/@color)};background-color: yellow;">
                            <xsl:value-of select="nombre/@gobierno"/>
                        </td>
                    </xsl:when>
                    <xsl:otherwise>
                        <td style="{concat('border: 1px solid ',../../nombre/@color)}">
                            <xsl:value-of select="nombre/@gobierno"/>
                        </td>
                    </xsl:otherwise>
                </xsl:choose>
                <td style="{concat('border: 1px solid ',../../nombre/@color)}">
                    <xsl:value-of select="capital"/>
                </td>
            </tr>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>